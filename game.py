import pygame
from pygame import gfxdraw
import sys

from client import Client
from shared import *

client = Client()

pygame.init()

size = width, height = 800, 600

screen = pygame.display.set_mode(size)
pygame.display.set_caption('Pysocketsnake')

opponents = []

'''
user_data: main player info
opponents: same but for opponents
'''
user = client.data[0]
opponents.extend(client.data[1])

speed = [1, 0]

clock = pygame.time.Clock()

# game and socket loop combined
while True:
    opponents = client.update(user)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                speed = [0, -1]
            if event.key == pygame.K_DOWN:
                speed = [0, 1]
            if event.key == pygame.K_LEFT:
                speed = [-1, 0]
            if event.key == pygame.K_RIGHT:
                speed = [1, 0]

    # change user's position
    user['position'] = update_xy(user['position'], speed)

    # snake hits the edge
    if user['position'][0] < 0:
        user['position'] = (760, user['position'][1])
    if user['position'][0] > width:
        user['position'] = (0, user['position'][1])
    if user['position'][1] < 0:
        user['position'] = (user['position'][0], 560)
    if user['position'][1] > height:
        user['position'] = (user['position'][0], 0)

    screen.fill((255, 255, 255))

    # Draw the player
    pygame.gfxdraw.aacircle(screen, *user['position'], 20, user['color'])
    pygame.gfxdraw.filled_circle(screen, *user['position'], 20, user['color'])

    # Draw opponents
    for opponent in opponents:
        pygame.gfxdraw.aacircle(screen, *opponent['position'], 20, opponent['color'])
        pygame.gfxdraw.filled_circle(screen, *opponent['position'], 20, opponent['color'])

    pygame.display.flip()

    clock.tick(30)
