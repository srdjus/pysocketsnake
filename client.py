import socket
import pickle

from shared import *

HOSTNAME = socket.gethostname()
PORT = 8888


class Client:
    def __init__(self):
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        '''
        client_data: (position, user_data, list of opponents)
        '''
        self.data = self.connect()

    '''
    this function will send a request to the server containing players,
    positions, after that we collect data about other users
    '''
    def call(self):
        res = receive_message(self.conn)

        return pickle.loads(res['message'])

    '''
    invoking user registration on server side, which will give us
    basic information about users
    '''
    def connect(self):
        self.conn.connect((HOSTNAME, PORT))
        name = bytes(input('Enter your username: '), 'utf-8')

        self.conn.send(convert_message(name))

        res = receive_message(self.conn)

        return pickle.loads(res['message'])

    '''
    client sends his data to the server and in 
    return other players data
    '''
    def update(self, user_data):
        self.conn.send(convert_message(pickle.dumps(user_data)))
        res = receive_message(self.conn)

        return pickle.loads(res['message'])
