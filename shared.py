import random

'''
I will wrap most of these inside suitable Classes,
for now this is enough
'''

# how many space for header value (10^3 - 1)
HEADER_LENGTH = 3

'''
    colors: blue, green, yellow, purple
'''
COLORS = [
    (93, 197, 232),
    (93, 232, 179),
    (255, 232, 115),
    (230, 138, 255)
]


# convert message in form suitable for sending to the client
def convert_message(msg):
    # check if message exceeds the limit
    if len(msg) >= 10 ** HEADER_LENGTH - 1:
        # handle error
        return

    formatted = bytes(f'{len(msg):<{HEADER_LENGTH}}', 'utf-8') + msg

    return formatted


def receive_message(socket):
    # message is always sent in bytes
    msg = b''

    try:
        data = socket.recv(HEADER_LENGTH)

        if not len(data):
            return False

        msg_len = int(data[:HEADER_LENGTH])

        msg = b''
        msg += data[HEADER_LENGTH:]
        msg += socket.recv(msg_len)

        return {'header': data[:HEADER_LENGTH], 'message': msg}

    except Exception as e:
        raise e


def format_message(user_data, msg_data):
    username = user_data['message'].decode('utf-8')
    msg = msg_data['message'].decode('utf-8')

    return f'{username} < {msg}'


def get_random_xy(width, height):
    x = 40 * random.randint(1, width / 40) - 40
    y = 40 * random.randint(1, height / 40) - 40

    return x, y


def update_xy(position, speed):
    temp = list(position)

    temp[0] += speed[0]
    temp[1] += speed[1]

    return tuple(temp)


def find_user(socket, users):
    for i, user in enumerate(users):

        if user['conn'] == socket:
            return i

    return -1


def user_public_data(user):
    return {
        '_id': user['_id'],
        'name': user['name'],
        'color': user['color'],
        'position': user['position']
    }
