import socket
import select
import pickle
from shared import *

HOSTNAME = socket.gethostname()
PORT = 8888

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOSTNAME, PORT))
s.listen(2)

socket_list = [s]
users = []

print(f'Server {HOSTNAME}:{PORT} is running...')

is_playing = False
food_xy = get_random_xy(800, 600)


while True:

    read_sockets, _, except_sockets = \
        select.select(socket_list, [], socket_list)

    for active_socket in read_sockets:

        # user registration
        if active_socket == s:
            # we have a new connection
            client_socket, address = s.accept()

            user_data = receive_message(client_socket)

            socket_list.append(client_socket)

            new_user = {
                '_id': len(users),
                'name': user_data["message"].decode("utf-8"),
                'conn': client_socket,
                'color': COLORS[len(users)],
                'position': get_random_xy(800, 600)
            }

            # retrieve existing users
            existing = [user_public_data(user) for user in users]

            users.append(new_user)

            print(f'{new_user["name"]} has joined.')

            game_data = convert_message(
                pickle.dumps([
                    user_public_data(new_user),
                    existing
                ]))

            client_socket.send(game_data)

        # receive a message from a registered user
        else:
            msg_data = receive_message(active_socket)

            # user's index
            i = find_user(active_socket, users)

            if not msg_data:
                # User disconnected
                socket_list.remove(active_socket)
                print(f'{users[i]["name"]} left the game.')
                del users[i]
                continue

            req = pickle.loads(msg_data['message'])
            '''
            update user's position
            return opponents
            '''
            users[i]['position'] = req['position']

            opponents = []

            for user in users:
                if user != users[i]:
                    opponents.append(user_public_data(user))

            active_socket.send(convert_message(
                pickle.dumps(opponents)))
