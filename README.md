**Pysocketsnake**

I wrote this with intention to present pygame socket implementation. It's known concept with few popular ways to handle specific use cases.

I did it with with few catches. For example, in the game loop main player (your machine) is rendered immediately. This fits better some use cases, it all depends. My intention was not to make a snake game, but to implement sockets in a specific way.

For server:
`python server.py`

For each client:
`python game.py`    
  